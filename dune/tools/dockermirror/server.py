from aiohttp import web
from concurrent import futures
import time
import asyncio
import docker
import re
from pathlib import Path
import hashlib
from collections import namedtuple
import yaml
from json.decoder import JSONDecodeError
import logging

ImageUpdate = namedtuple('ImageUpdate',['image','timestamp','mirrors'])

class IllegalImageError(Exception):
    pass


class DockerMirror:


    class Worker:

        def __init__(self,mirrorer):
            self.cancelled = False

        def cancel(self):
            self.cancelled = True

        def process_image_update(self,task):

            client = docker.from_env()
            image = client.images.pull(task.image)

            for mirror in task.mirrors:
                if self.cancelled:
                    return
                image.tag(mirror)
                client.images.push(mirror)



    def __init__(self,app,config):
        self.app = app
        self.logger = app.logger
        self.executor = futures.ProcessPoolExecutor(max_workers=1)
        self.queue = asyncio.Queue(loop=app.loop)
        self.processor = app.loop.create_task(self.process_image_update())
        self.state = {}
        if 'spool_dir' in config:
            self.spool_dir = Path(config['spool_dir'])
        else:
            self.spool_dir = None
        self.image_specs = config['image_specs']
        self.token = config['token']
        self.spool_file_template = config.get('spool_file_template','{}')
        self.worker = self.Worker(self)

    def get_mirrors(self,image):
        for pattern, mirrors in self.image_specs.items():
            if not isinstance(mirrors,list):
                mirrors = [mirrors]
            match = re.match(pattern,image)
            if match:
                return [ match.expand(mirror) for mirror in mirrors ]
        raise IllegalImageError("Could not find valid mirror spec for image {}".format(image))

    def spool_files(self,image):
        task_hash =  hashlib.sha256()
        task_hash.update(image.encode())
        task_hash = task_hash.hexdigest()
        spool_file = self.spool_file_template.format(task_hash)
        for host in self.spool_dir.iterdir():
            yield host / spool_file

    async def process_image_update(self):
        try:
            while True:

                task = await self.queue.get()

                if self.state[task.image] > task.timestamp:
                    continue

                try:
                    self.logger.info("Processing image %s",task.image)
                    await self.app.loop.run_in_executor(self.executor,self.worker.process_image_update,task)
                    self.logger.info("Image %s successfully mirrored",task.image)
                except Exception as e:
                    self.logger.exception("Error while processing {}:".format(task))

                self.queue.task_done()

        except asyncio.CancelledError:
            pass

    async def update_image(self,image):
        mirrors = self.get_mirrors(image)
        timestamp = time.monotonic()
        self.state[image] = timestamp
        await self.queue.put(ImageUpdate(image=image,mirrors=mirrors,timestamp=timestamp))

        # notify Docker daemons on build servers about new image
        if self.spool_dir:
            for spool_file in self.spool_files(image):
                with spool_file.open('w') as spool:
                    spool.write('{}\n'.format(image))


    def close(self):
        self.processor.cancel()
        self.worker.cancel()

    async def wait_closed(self):
        await self.processor
        self.executor.shutdown()


def dockermirror(config):
    async def dockermirror(app):
        app['dockermirror'] = DockerMirror(app,config)
        yield
        app['dockermirror'].close()
        await app['dockermirror'].wait_closed()

    return dockermirror

async def dockermirror_handler(request):
    mirror = request.app['dockermirror']
    try:
        if request.headers['token'] != mirror.token:
            raise web.HTTPForbidden()
    except KeyError:
        raise web.HTTPForbidden()

    try:
        payload = await request.json()
    except JSONDecodeError:
        raise web.HTTPNotAcceptable(reason="Request body must be valid JSON")
    if not (isinstance(payload,list) and all(isinstance(v,str) for v in payload)):
        raise web.HTTPNotAcceptable(reason="JSON payload must be a list of strings denoting Docker images")

    try:
        for task in payload:
            await mirror.update_image(task)
    except IllegalImageError as e:
        raise web.HTTPNotAcceptable(reason=e)
    return web.Response(text="accepted\n")



def serve():

    import sys
    try:
        configfile = sys.argv[1]
    except IndexError:
        configfile = '/etc/dockermirror.yml'

    logging.basicConfig(level=logging.INFO)

    with open(configfile) as configfile:
        config = yaml.safe_load(configfile)['dockermirror']

    app = web.Application()
    app.cleanup_ctx.append(dockermirror(config))
    app.add_routes([web.post('/update-images',dockermirror_handler)])

    web.run_app(app,port=config['port'])
