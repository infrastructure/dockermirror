# Docker Mirroring Service

This project provides a simple HTTP server that listens on a given port for JSON requests that
contain a list of docker image names with tags. For each image name it does the following:

- It compares the image name against a list of regular expressions to determine whether the image
  should be processed. It hen constructs a list of mirror image names for that image.
- It optionally writes the image name to spool files in a directory hierarchy, which can be used by
  a cron job or a similar utility to pre-fill a cache with those Docker images.
- It pulls the given image to the local Docker daemon.
- It tags it with all of the mirror image names and pushes the image to those mirror registries.

Requests must authorize by providing a simple token in a header called `token`.

## Usage

The server gets installed as an entrypoint called `dockermirror` and takes a single optional
argument, the name of the config file. If the config file name is omitted, it defaults to
`/etc/dockermirror.yml`.

Here is a sample configuration file:

```yaml
dockermirror:
  # this key contains a dict of regular expressions for input images and a nested list
  # of mirror image names. The mirror names can use the capture groups defined in the
  # regular expression.
  image_specs:
    '^registry.dune-project.org/docker/images/ci/([^/]+:[^/]+)$':
      - 'duneci/\1'
    '^registry.dune-project.org/docker/images/service/([^/]+:[^/]+)$':
      - 'duneci/service/\1'
  # the port that the server will listen on
  port: 8421
  # the token used for authorization
  token: ABC123
```
